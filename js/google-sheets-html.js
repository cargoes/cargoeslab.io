/*!
 * 
 * Google Sheets To HTML v0.9b
 * 
 * To use, simply replace the "tq?key=" value in the
 * URL below with your own unique Google document ID
 * 
 * The Google document's sharing must be set to public
 * 
 */

google.charts.load('current', {packages: ['table']});
var visualization;

function drawVisualization() {
    // var query = new google.visualization.Query('https://docs.google.com/spreadsheets/d/1y8QRGUT0bb0Wx6lAHbMjNxU7Zs96WDJE9SXRiuQQfJc/gviz/tq?output=html&usp=sharing');
    var query = new google.visualization.Query('https://docs.google.com/spreadsheets/d/1Cex7RdnhZqwBGoTBuwC2hg6edvDSTEX5JhjJXwOq3m0/gviz/tq?output=html&usp=sharing&headers=1');
    // query.setQuery('SELECT A, B, C, D label A "Duration", B "Song", C "Requested By", D "URL"');
    query.setQuery('SELECT A, B, C, D, E, F');
    query.send(handleQueryResponse);
}
let r,d;
function handleQueryResponse(response) {
    // console.log(JSON.stringify(response));
    console.log(response);
    r = response
    // console.log(parse(response));
    if (response.isError()) {
        alert('There was a problem with your query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
        return;
    }
    var data = response.getDataTable();
    visualization = new google.visualization.Table(document.getElementById('table'));
    visualization.draw(data, {
        allowHtml: true,
        legend: 'bottom'
    });
}
google.setOnLoadCallback(drawVisualization);
// google.setOnLoadCallback(function(){handleQueryResponse(
//     Object.assign(Object.create(google.visualization.QueryResponse.prototype), _response)
//     // Object.assign(Object.create(google.visualization.QueryResponse.prototype), res)
//     )});

let _response = {
    "m4": "2037479830",
    "Ta": {"cols":[{"id":"A","label":"Тип","type":"string"},{"id":"B","label":"Линия","type":"string"},{"id":"C","label":"Город","type":"string"},{"id":"D","label":"Дата","type":"string"},{"id":"E","label":"Вес","type":"number","pattern":"0 т."},{"id":"F","label":"Контакты","type":"string"}],"rows":[{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Днепропетровск"},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Хмельницкий"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Харьков"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Киев "},null,{"v":24,"f":"24 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Харьков "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"EVERGREEN"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"EVERGREEN"},{"v":"Днепр "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"HAPAG"},{"v":"Киев "},null,{"v":24.3,"f":"24 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MAERSK"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MAERSK"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Умань"},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"OOCL"},{"v":"Харьков "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"ZIM"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"ZIM"},{"v":"Харьков"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"20 DV"},{"v":"HAPAG"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"20 DV"},{"v":"MAERSK"},{"v":"Киев"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"20 DV"},{"v":"MSC"},{"v":"Киев"},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[null,null,null,null,null,{"v":"0682314080 Татьяна, Карго Транс"}]}]},
    "errors": [],
    "warnings": [],
    "Lva": "0.6",
    "DY": "ok"
}

// const res = `{"m4":"2037479830","Ta":"{"cols":[{"id":"A","label":"Тип","type":"string"},{"id":"B","label":"Линия","type":"string"},{"id":"C","label":"Город","type":"string"},{"id":"D","label":"Дата","type":"string"},{"id":"E","label":"Вес","type":"number","pattern":"0 т\\".\\""},{"id":"F","label":"Контакты","type":"string"}],"rows":[{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Днепропетровск"},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Хмельницкий"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Харьков"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Киев "},null,{"v":24,"f":"24 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"COSCO"},{"v":"Харьков "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"EVERGREEN"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"EVERGREEN"},{"v":"Днепр "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"HAPAG"},{"v":"Киев "},null,{"v":24.3,"f":"24 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MAERSK"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MAERSK"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Умань"},null,{"v":26,"f":"26 т."},{"v":" 0684992626 Алла Карго Транс"}]},{"c":[{"v":"40 HC"},{"v":"MSC"},{"v":"Киев "},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"OOCL"},{"v":"Харьков "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"ZIM"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[{"v":"40 HC"},{"v":"ZIM"},{"v":"Харьков"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"20 DV"},{"v":"HAPAG"},{"v":"Киев "},null,{"v":26,"f":"26 т."},{"v":"0684992626 Алла Карго Транс"}]},{"c":[{"v":"20 DV"},{"v":"MAERSK"},{"v":"Киев"},null,null,{"v":"Денис Cargo Trans +380975664343"}]},{"c":[{"v":"20 DV"},{"v":"MSC"},{"v":"Киев"},null,{"v":25,"f":"25 т."},{"v":"0931211838 Карго Транс Сергей "}]},{"c":[null,null,null,null,null,{"v":"0682314080 Татьяна, Карго Транс"}]}]}","errors":[],"warnings":[],"Lva":"0.6","DY":"ok"}`
// let _response = JSON.parse(res)

// function parse(myObj){
//     // Stringify the object using a replacer function that will explicitly
// // turn functions into strings
// return JSON.stringify(myObj, function(key, val) {
//     return (typeof val === 'function') ? '' + val : val;
// });
// }
